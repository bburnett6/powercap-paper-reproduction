
CC=gcc
CXX=g++
NVCC=nvcc
PAPILIB := -L/$(PAPI_DIR)/lib -lpapi
PAPIINC := -I/$(PAPI_DIR)/include

dgemm: dgemm.cpp
	$(CXX) -Wall -o dgemm.x dgemm.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas 

dgemv: dgemv.cpp
	$(CXX) -Wall -o dgemv.x dgemv.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas

sgemm: sgemm.cpp
	$(CXX) -Wall -o sgemm.x sgemm.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas

sgemv: sgemv.cpp
	$(CXX) -Wall -o sgemv.x sgemv.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas

zgemm: zgemm.cpp
	$(CXX) -Wall -o zgemm.x zgemm.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas

zgemv: zgemv.cpp
	$(CXX) -Wall -o zgemv.x zgemv.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas

all:
	$(CXX) -Wall -o dgemm.x dgemm.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas
	$(CXX) -Wall -o dgemv.x dgemv.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas
	$(CXX) -Wall -o sgemm.x sgemm.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas
	$(CXX) -Wall -o sgemv.x sgemv.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas
	$(CXX) -Wall -o zgemm.x zgemm.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas
	$(CXX) -Wall -o zgemv.x zgemv.cpp $(PAPIINC) $(PAPILIB) -lpthread -lopenblas
