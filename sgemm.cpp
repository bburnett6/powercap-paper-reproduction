

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <cblas.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#include "papi.h"
#define MAX_powercap_EVENTS 64

bool stop_monitor = 0, lower_powerlimit = 0;
pthread_mutex_t wait_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t start_next_experiment = PTHREAD_COND_INITIALIZER;

void *sgemm_test(void *ptr);
void *background_monitor_fxn(void *ptr);

typedef struct{int m; int n; int k;} inputdim;

int main(int argc, char* argv[])
{

  if (argc < 4) 
  {
    printf("Enter m, n, k\n");
    printf("Ex: ./sgemm m n k\n");
    exit(1);
  }
  inputdim *fxn_inp;
  fxn_inp = (inputdim*)malloc(sizeof(inputdim));
  fxn_inp->m = atoi(argv[1]);
  fxn_inp->n = atoi(argv[2]);
  fxn_inp->k = atoi(argv[3]);

  int expret, monret;
  pthread_t thread1, thread2;

  //start monitor
  monret = pthread_create(&thread1, NULL, background_monitor_fxn, NULL);
  if (monret < 0)
    fprintf(stderr, "Monitor returned non-zero: %d\n", monret);
    
  //start experiment
  expret = pthread_create(&thread2, NULL, sgemm_test, (void*)fxn_inp);
  if (expret < 0)
    fprintf(stderr, "Experiment returned non-zero: %d\n", expret);
  
  pthread_join(thread2, NULL);
  pthread_join(thread1, NULL);

  return(0);

}

void *sgemm_test(void *ptr)
{
  
  int i;
  inputdim *input = (inputdim*)ptr;
  int m = input->m;
  int n = input->n;
  int k = input->k;
  int sizeofa = m * k;
  int sizeofb = k * n;
  int sizeofc = m * n;
  double alpha = 1.2;
  double beta = 0.001;

  float* A = (float*)malloc(sizeof(float) * sizeofa);
  float* B = (float*)malloc(sizeof(float) * sizeofb);
  float* C = (float*)malloc(sizeof(float) * sizeofc);

  //srand((unsigned)time(NULL));
  srand(1234);

  for (i=0; i<sizeofa; i++)
    A[i] = (rand()%100)/10.0;

  for (i=0; i<sizeofb; i++)
    //B[i] = i%3+1;
    B[i] = (rand()%100)/10.0;

  for (i=0; i<sizeofc; i++)
    //C[i] = i%3+1;
    C[i] = (rand()%100)/10.0;

  //sync threads here so matrix initialization is not the focus of the test
  pthread_cond_signal(&start_next_experiment);
  //and then sleep to get some baseline readings
  usleep(100000);

  for (int i = 0; i < 6; i++) 
  {
    cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, n, k, alpha, A, m, B, k, beta, C, m);
    lower_powerlimit = 1;
    pthread_cond_wait(&start_next_experiment, &wait_mutex);
  }

  usleep(100000); //sleep a little
  stop_monitor = 1;

  free(A);
  free(B);
  free(C);
  return ptr;
}

void *background_monitor_fxn(void *ptr)
{
  int retval,cid,powercap_cid=-1,numcmp;
  int EventSet = PAPI_NULL;
  long long values[MAX_powercap_EVENTS], initial_values[MAX_powercap_EVENTS];
  int limit_A=0, limit_B=0, energy_uj=0;
  int num_events=0, num_limits=0;
  int code;
  char event_names[MAX_powercap_EVENTS][PAPI_MAX_STR_LEN];
  int r;
  long long start_time,before_time,after_time;
  double elapsed_time,total_time;

  const PAPI_component_info_t *cmpinfo = NULL;

  /* PAPI Initialization */
  retval = PAPI_library_init( PAPI_VER_CURRENT );
  if ( retval != PAPI_VER_CURRENT )
    fprintf(stderr, "Papi init failed: %d\n", retval);;

  numcmp = PAPI_num_components();

  for( cid=0; cid<numcmp; cid++ ) {

    if ( ( cmpinfo = PAPI_get_component_info( cid ) ) == NULL )
      fprintf(stderr, "papi get component info failed: %d\n", retval);;

    if ( strstr( cmpinfo->name,"powercap" ) ) {
      powercap_cid=cid;
      if ( cmpinfo->disabled )
        fprintf(stderr, "powercap component disabled: %s\n",
          cmpinfo->disabled_reason );
      break; //found powercap so break
    }
  }

  //test if component works
  if ( cid==numcmp )
  {
    fprintf(stderr, "No powercap component.\n");
    exit(1);
  } 
  
  if ( cmpinfo->num_cntrs==0 )
  {
    fprintf(stderr,"No counters in the powercap component\n");
    exit(1);
  }

  //create eventset
  retval = PAPI_create_eventset( &EventSet );
  if ( retval != PAPI_OK )
  {
    fprintf(stderr, "PAPI_create_eventset() failed: %d\n" ,retval );
  }

  //add events
  code = PAPI_NATIVE_MASK;
  r = PAPI_enum_cmp_event( &code, PAPI_ENUM_FIRST, powercap_cid );


  //find events we are interested in and place them into limit_map
  while ( r == PAPI_OK ) {
    retval = PAPI_event_code_to_name( code, event_names[num_events] );
    if ( retval != PAPI_OK ) 
      fprintf(stderr, "PAPI_event_code_to_name() failed: %d\n", retval );

    retval = PAPI_add_event(EventSet, code);
    if (retval != PAPI_OK)
      break; // We've hit an event limit

    //we are interested in power limits and energy_uj
    if (!(strstr(event_names[num_events],"SUBZONE")) && (strstr(event_names[num_events],"POWER_LIMIT_A"))) {
      limit_A = num_events;
      num_limits++;
    }
    if (!(strstr(event_names[num_events],"SUBZONE")) && (strstr(event_names[num_events],"POWER_LIMIT_B"))) {
      limit_B = num_events;
      num_limits++;
    }
    if (!(strstr(event_names[num_events],"SUBZONE")) && strstr(event_names[num_events], "ENERGY_UJ:ZONE0")) {
      energy_uj = num_events;
      num_limits++;
    }

    num_events++;
    r = PAPI_enum_cmp_event( &code, PAPI_ENUM_EVENTS, powercap_cid );
  }

  //Wait for memory initialization of test to be finished
  pthread_cond_wait(&start_next_experiment, &wait_mutex);
  //test thread will sleep for a bit to let this thread get base line readings

  //get initial values and write starting powerlimit
  retval = PAPI_start( EventSet );
  if ( retval != PAPI_OK )
  {
    fprintf(stderr, "PAPI_start() failed: %d\n",retval );
    exit(1);
  }

  retval = PAPI_read( EventSet, values );
  if ( retval != PAPI_OK )
    fprintf(stderr, "PAPI_read() failed: %d\n", retval);
  
  for(int i = 0; i < MAX_powercap_EVENTS; i++)
    initial_values[i] = values[i];

  values[limit_B] = 120 * 1e6; //120uW
  retval = PAPI_write(EventSet, values);
  if ( retval != PAPI_OK )
    fprintf(stderr, "Error Writing starting power limit values. Error %d\n", retval);


  retval = PAPI_stop(EventSet, values);
  if (retval != PAPI_OK)
    fprintf(stderr, "PAPI_stop() failed: %d\n", retval);



  //###################################################

  // Output file
  FILE *out_file = fopen("./results/sgemm.csv", "w");

  //#################################################

  //Begin monitor
  start_time=PAPI_get_real_nsec();
  fprintf(out_file, "elapsed_time(s),cpu_power(W),cpu_power_limitA(W),cpu_power_limitB \n");
  while(!stop_monitor)
  {
      before_time = PAPI_get_real_nsec();
      retval = PAPI_start(EventSet);
      if (retval != PAPI_OK) {
          fprintf(stderr,"PAPI_start() for powercap failed. Error: %d\n", retval);
          exit(1);
      }

      usleep(10000);

      //check if we should modify the powerlimit vals
      if (lower_powerlimit)
      {
        //Update the power limit values to current - 10W (values in uW so need to convert)
        //printf("powercap val before: %lld\n", values[limit_A]);
        values[limit_B] = values[limit_B] - (10 * 1e6);
        //printf("powercap val supposed: %lld\n", values[limit_A]);
        retval = PAPI_write(EventSet, values);
        if ( retval != PAPI_OK )
          fprintf(stderr, "Error Writing new power limit values. Error %d\n", retval);
        //retval = PAPI_read(EventSet, values);
        //if (retval != PAPI_OK)
        //  fprintf(stderr, "Error Reading new powerlimit values. Error %d\n", retval);
        //printf("powercap val after: %lld\n", values[limit_A]);

        pthread_cond_signal(&start_next_experiment);
        lower_powerlimit = 0;
      }

      // Stop counting and record values
      after_time = PAPI_get_real_nsec();
      retval = PAPI_stop(EventSet, values);
      if (retval != PAPI_OK) {
          fprintf(stderr, "PAPI_stop() powercap failed\n");
      }

      total_time = ((double)(after_time - start_time)) / 1.0e9;
      elapsed_time = ((double)(after_time - before_time)) / 1.0e9;

      fprintf(out_file,"%.4f,%.3f,%.3f,%.3f\n",
              total_time,
              ((double)values[energy_uj] / 1.0e6) / elapsed_time,
              ((double)values[limit_A] / 1.0e6),
              ((double)values[limit_B] / 1.0e6));

      fflush(out_file);

  }

  //reset the power limits to the original values
  retval = PAPI_start(EventSet);
  if ( retval != PAPI_OK )
    fprintf(stderr, "Error starting power limit event. Error %d\n", retval);
  values[limit_A] = initial_values[limit_A];
  retval = PAPI_write(EventSet, values);
  if ( retval != PAPI_OK )
    fprintf(stderr, "Error Writing starting power limit values. Error %d\n", retval);
  retval = PAPI_stop(EventSet, values);
  if (retval != PAPI_OK) {
    fprintf(stderr, "PAPI_stop() power limit failed\n");
  }

  return ptr;
}