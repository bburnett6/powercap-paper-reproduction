import numpy as np 
import matplotlib.pyplot as plt 


def main():
	basedir = 'results'
	tests = ['sgemv', 'sgemm', 'dgemv', 'dgemm', 'zgemv', 'zgemm']
	files = [f'{basedir}/{d}.csv' for d in tests]

	timedata = {} 
	cpudata = {}
	limitdata = {}
	for i, test in enumerate(tests):
		timedata[test] = np.loadtxt(files[i], delimiter=',', skiprows=1, usecols=(0,))
		cpudata[test] = np.loadtxt(files[i], delimiter=',', skiprows=1, usecols=(1,))
		limitdata[test] = np.loadtxt(files[i], delimiter=',', skiprows=1, usecols=(3,))

	
	#https://stackoverflow.com/questions/6963035/pyplot-axes-labels-for-subplots
	fig, axs = plt.subplots(3, 2, figsize=(14, 10))
	for i, ax in enumerate(axs.flat):
		ax.plot(timedata[tests[i]], cpudata[tests[i]], label='CPU Power')
		ax.plot(timedata[tests[i]], limitdata[tests[i]], label='Powercap Level')
		ax.set_title(f"{tests[i]}".upper())

	plt.setp(axs[-1, :], xlabel='Elapsed Time (s)')
	plt.setp(axs[:, 0], ylabel='Power (W)')
	#https://stackoverflow.com/questions/9834452/how-do-i-make-a-single-legend-for-many-subplots-with-matplotlib
	handles, labels = axs.flat[1].get_legend_handles_labels()
	axs.flat[1].legend(handles, labels)
	#https://stackoverflow.com/questions/7066121/how-to-set-a-single-main-title-above-all-the-subplots-with-pyplot
	fig.suptitle('Effect of PAPI Powercap on OpenBLAS Subroutines', fontsize=24)
	plt.show()

	"""
	plt.plot(time_data, cpu_data, label="CPU Power")
	plt.plot(time_data, limitA_data, label="Power Limit")
	plt.title("PAPI Power Limit on DGEMM (ppc)")
	plt.xlabel("Elapsed Time (s)")
	plt.ylabel("Power (W)")
	plt.legend()
	plt.show()
	"""

if __name__ == '__main__':
	main()